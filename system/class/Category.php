<?php

class Category
{
    public $category_id;
    public $category;
    public $sum = 0;

    public function __construct($category_id,$category,$sum){

        $this->cat_id      = $category_id;
        $this->cat_name    = $category;
        $this->cat_count   = $sum;

    }

    public function setCategoryName($category)
    {
        $this->cat_name = $category;
    }

    public function setCountCategories($sum)
    {
        $this->cat_count = $sum;
    }

    public function getCategoryId(){
        return $this->cat_id;
    }

    public function getCategoryName(){
        return $this->cat_name;
    }

    public function getCountCategories(){
        return $this->cat_count;
    }

}